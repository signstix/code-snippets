var i;
document.body.innerHTML = '';

var link = document.createElement('a');
var linkText = document.createTextNode("Tweets by <company-name-here>");

link.href = "https://twitter.com/<company-name-here>";
link.className="twitter-timeline";

link.setAttribute('data-width', '100%');
link.setAttribute('data-chrome', 'noheader nofooter');
link.appendChild(linkText);
document.body.appendChild(link);

var script = document.createElement('script');
script.src = 'https://platform.twitter.com/widgets.js';
script.async = true;
document.body.appendChild(script);

if(!i) {
  i = true;
  dispatchEvent(new Event('load'));
}
