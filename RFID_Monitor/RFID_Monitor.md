# RFID Monitor #

This is a test/demo for the SignStix serial interface.  Requires SignStix Player version 2.11.x or above 

It assumes that the SignStix device is connected to a ThingMagic Nano RFID reader via a CH34xUART serial device plugged into a USB port on the SignStix device.

As tags are read, the web page is updated to show the current tags and the number of times each of them has been read.

It also shows the current strength of the reads, giving an indication of the tags' proximity to the RFID aerial.

The code is structured as follows:

1. USER SECTION
    * RfidClient
1. LIBRARY
    * ThingMagic-specific code
    * Generic code
1. MAIN PROGRAM
    * State
    * Top-level functions, notably startSerial()
1. HTML GENERATION
    * This code generates the HTML elements in which the results of the RFID reads are displayed.
	
The library code makes calls into the RfidClient to get configuration information (e.g. the USB serial device to connect to, the required RFID read power).  

The library code also invokes call-backs on the RfidClient to report significant events e.g.

- A tag has been read.
- A tag has been added-to or removed-from the current set.
- An RFID reader temperature has been measured.
	
To re-use this code in other applications, copy all the code, then customise RfidClient in the USER SECTION.  The HTML GENERATION section can also be modified.

