/******************** USER SECTION ********************/

/******************** Customise RfidClient for your Application ********************/

/* The RfidClient is informed at various points in the application, giving it a chance
   to respond e.g. to triggered tags.  Customise the member functions as necessary.  */
function RfidClient() {
}

/* Return the serial device that we want to communicate with. */
RfidClient.prototype.getSerialDevice = function() {
  var serialDevice = new SerialDevice();
  serialDevice.setVendorId("1A86"); /* Decimal 6790 */
  serialDevice.setProductId("7523"); /* Decimal 29987 */
  serialDevice.setDriverName("ch34");
  return serialDevice;
};

/* Populate the argument KnownTagGroup with the items/tags that we know about. */
RfidClient.prototype.populateTags = function(knownTagGroup) {
  knownTagGroup.addItem("Black Coaster", "E20000158612007923402984", 1);
  knownTagGroup.addItem("Red Coaster", "E20000158612008123402991", 2);
};

/* Return the number of milliseconds without a read that will cause a tag to be removed. */
RfidClient.prototype.getRemoveDetectionPeriodMs = function() {
  return 500;
};

/* Return the number of reads that a newly active tag must get before
   it's considered 'stable' and therefore available for promotion.  */
RfidClient.prototype.getStabilityThreshold = function() {
  return 40;
};

/* Decide if a read of the argument tag ID at the argument strength (0 - 255) should be ignored.  
   This can be used to filter out weak reads that might otherwise pollute the data. */
RfidClient.prototype.getShouldIgnoreRead = function(tagId, readStrength) {
  if (readStrength < 200) return true;
  return false;
};

/* Return the read power in dBm - can be one of {5, 10, 15, 18, 20, 22, 25} */
RfidClient.prototype.getReadPower = function() {
  return 22;
};

/* Show the argument status message. */
RfidClient.prototype.showStatus = function(status) {
  document.getElementById("status").innerHTML = "Status: " + status;
};

/* Called when the attached devices have been established. */
RfidClient.prototype.onFoundDevices = function(devicesInfo) {
  document.getElementById("deviceInfo").innerHTML = "Devices: " + devicesInfo;
};

/* Called when the required serial device has been found. */
RfidClient.prototype.onFoundDevice = function(deviceStr) {
  document.getElementById("foundDevice").innerHTML = "Device: " + deviceStr;
};

/* Called when a connection (with the argument ID) to the serial device has been established.  */
RfidClient.prototype.onConnection = function(connId) {
  document.getElementById("connection").innerHTML = "Connection ID: " + connId;
};

/* Called when an attempt to get permission over the serial device has completed. */
RfidClient.prototype.onPermission = function(granted) {
  document.getElementById("permission").innerHTML = "Permission granted: " + granted;
};

/* Called when the RFID read power (in dBm) has been set. */
RfidClient.prototype.onReadPowerSet = function(readPower) {
  document.getElementById("readPower").innerHTML = "Read power: " + readPower + " dBm";
};

/* Called when a potential read was ignored (e.g. due to low read strength). */
RfidClient.prototype.onReadIgnored = function(totalIgnored) {
  document.getElementById("ignored").innerHTML = "Total reads ignored: " + totalIgnored;
};

/* Called when a reader temperature has been measured. Typically this only happens at the start. */
RfidClient.prototype.onTemperature = function(temp) {
  document.getElementById("temperature").innerHTML = "Initial temperature: " + temp;
  SignStixDebug.info("Found temperature " + temp);
};

/* Called when the known tags have been established. */
RfidClient.prototype.onKnownTags = function(knownTagGroup) {
  var msg = knownTagGroup.getHtmlReport();
  document.getElementById("knownTags").innerHTML = msg;
};

/* Called whenever a tag is read. */
RfidClient.prototype.onTagRead = function(tagData, tagDatabase) {
  var msg = tagDatabase.getHtmlReport();
  document.getElementById("tagDump").innerHTML = msg;
};

/* Called when the argument tag has been added to the current set.
   The knownTag may be null if the tag is not associated with a known object. */
RfidClient.prototype.onTagAdded = function(tagData, knownTag) {
};

/* Called when the argument known TagData has been added to the current set and
   is being 'promoted'. */
RfidClient.prototype.onTagAddedPromote = function(tagData, knownTag) {
  var info = "Item was put down: " + knownTag.getName();
  SignStixDebug.info(info);
  document.getElementById("feedback").innerHTML = info;
};

/* Called when the argument TagData has been removed from the current set.
   The knownTag may be null if the tag is not associated with a known object. */
RfidClient.prototype.onTagRemoved = function(tagData, knownTag) {
};

/* Called when the argument known TagData has been removed from the current set and is
   being 'promoted'. */
RfidClient.prototype.onTagRemovedPromote = function(tagData, knownTag) {
  var itemName = knownTag.getName();
  var numRemoved = knownTag.getNumRemoved();
  var info = "You picked up:\n" + itemName + "\n(" + numRemoved + ")";
  SignStixDebug.info(info);
  document.getElementById("feedback").innerHTML = info;
};


/******************** LIBRARY ********************/

/******************** ThingMagic-Specific Library ********************/

/* Constants for calculating ThingMagic CRC. */
var CRC_TABLE = [0x0000, 0x1021, 0x2042, 0x3063, 0x4084, 0x50a5,
  0x60c6, 0x70e7, 0x8108, 0x9129, 0xa14a, 0xb16b, 0xc18c, 0xd1ad, 0xe1ce, 0xf1ef
];

/* Mapping of RFID read power to the corresponding command. */
var RFID_READ_POWER_COMMANDS = {
  5: "FF029201F440AD",
  10: "FF029203E842B1",
  15: "FF029205DC4485",
  18: "FF029207084651",
  20: "FF029207D04689",
  22: "FF02920A8C4BD5",
  25: "FF029209C4489D"
};

/* Constant to mean that the response contains a tag. */
var RESPONSE_HAS_TAG = 1;

/* Constant to mean that the response contains a temperature. */
var RESPONSE_HAS_TEMPERATURE = 2;

/* Constant to mean that the response doesn't contain anything we care about. */
var RESPONSE_NOT_RECOGNISED = 3;

/* Index in a tag-read response where we expect to find the read strength. */
var READ_STRENGTH_INDEX = 12;

/* Index in a tag-read response where we expect to find the size of the embedded data. */
var EMBEDDED_SIZE_INDEX = 24;

/* Index in a tag-read record (minus the embedded data) where we expect to
   find the size of the EPC tag data. */
var EPC_SIZE_INDEX = 27;

/* Calculate the 2-byte CRC of the argument data at the argument offset where
   'length' is the number of bytes to process. */
function calculateCrc(data, offset, length) {
  var crc = 0xffff;

  for (i = offset; i < offset + length; i++) {
    crc = ((crc << 4) | ((data[i] >> 4) & 0xf)) ^ CRC_TABLE[crc >> 12];
    crc &= 0xffff;
    crc = ((crc << 4) | ((data[i] >> 0) & 0xf)) ^ CRC_TABLE[crc >> 12];
    crc &= 0xffff;
  }
  return crc;
}

/**
 * Parse a tag ID from the argument ThingMagic response byte array (which is known to contain a tag).
 */
function parseTagFromResponse(response, length) {
  var embeddedSizeHi = response[EMBEDDED_SIZE_INDEX];
  var embeddedSizeLo = response[EMBEDDED_SIZE_INDEX + 1];
  var embeddedBits = ((embeddedSizeHi & 0xFF) << 8) | (embeddedSizeLo & 0xFF);
  var embeddedBytes = embeddedBits / 8;

  var epcSizeHi = response[EPC_SIZE_INDEX + embeddedBytes];
  var epcSizeLo = response[EPC_SIZE_INDEX + 1 + embeddedBytes];
  var epcBits = ((epcSizeHi & 0xFF) << 8) | (epcSizeLo & 0xFF);
  var epcBytes = epcBits / 8;

  var epcStartIndex = 31 + embeddedBytes;
  var epcIdSize = epcBytes - 4; /* Subtract the PC length and the tag CRC */
  var tagId = bytesToHex(response, epcStartIndex, epcIdSize);
  return tagId;
}

/* Attempt to parse the temperature from the argument ThingMagic response byte array
   (which is known to contain a temperature). */
function parseTempFromResponse(response, length) {
  var tempByte = response[5];
  return tempByte;
}

/* Attempt to parse the response byte array.  Return one of {RESPONSE_HAS_TAG, 
   RESPONSE_HAS_TEMPERATURE, RESPONSE_NOT_RECOGNISED}. */
function parseResponse(response, length) {
  var opCode = response[2];

  var crc = calculateCrc(response, 1, length - 3);
  if ((response[length - 2] != (crc >> 8)) ||
    ((response[length - 1] & 0xFF) != (crc & 0xFF))) {
    return RESPONSE_NOT_RECOGNISED;
  }

  if (opCode == 0x22 && response[1] == 0x28) {
    return RESPONSE_HAS_TAG;
  }

  if (opCode == 0x72 && response[1] == 0x01) {
    return RESPONSE_HAS_TEMPERATURE;
  }

  return RESPONSE_NOT_RECOGNISED;
}

/* Find the read power command corresponding to the argument read power, 
   which should be one of {5, 10, 15, 20, 25}. */
function getReadPowerCommandFor(readPower) {
  var res = RFID_READ_POWER_COMMANDS[readPower];
  if (res == null) {
    SignStixDebug.error("No read-power command found for read power (" + readPower + ")");
  }
  return res;
}

/******************** class ThingMagicRunner ********************/

/* The current runner - needed so that a global-scope callback can refer to a specific instance. */
var gThingMagicRunner = null;

/* Called when data is received for the current runner. The 'hexData' is a hex string. */
function globalOnDataRead(connId, hexData) {
  gThingMagicRunner.onDataRead(connId, hexData);
}

/* A 'ThingMagicRunner' arranges the sending of instructions as well as receipt of data back from the RFID reader. */
function ThingMagicRunner(tagDatabase, rfidClient, connectionId) {
  this.mTagDatabase = tagDatabase;
  this.mRfidClient = rfidClient;
  this.mConnectionId = connectionId;
  this.mInstructionSet = null;
  this.mHandler = null;
  this.mRegularUpdateIntervalMs = 100; /* How many millis to wait between checking for removed tags. */
  gThingMagicRunner = this;
}

/* Start the ThingMagic RFID reader doing its continuous multi-tag read. */
ThingMagicRunner.prototype.start = function() {

  /* Set up the handler that will process tag reads. */
  var manager = new ThingMagicManager(this.mTagDatabase, this.mRfidClient);
  this.mHandler = new ThingMagicHandler(manager);

  /* Arrange for a stop-read command to be called prior to connection close. */
  SignStixSerial.writePreClose(this.mConnectionId, "FF032F0000025E86");
  SignStixSerial.startReading(this.mConnectionId, "globalOnDataRead");

  /* Set-up instructions to make the ThingMagic RFID reader start reading multiple tags. */
  this.mInstructionSet = [
    "FF00031D0C",
    "FF000C1D03",
    "FF00681D67",
    "FF026A010E2E40",
    "FF04060001C200A460",
    "FF026A01122E5C",
    "FF026A01032E4D",
    "FF016105BDB8",
    "FF026A010C2E42",
    "FF026A010D2E43",
    "FF00631D6C",
    "FF00721D7D", /* Temperature */
    "FF00671D68",
    "FF00711D7E",
    "FF0197084BB5", /* Region - EU3 */
    "FF00721D7D", /* Temperature */
    "FF016201BEBC",
    getReadPowerCommandFor(this.mRfidClient.getReadPower()),
    "FF02940A8C2B13",
    "FF016201BEBC",
    "FF00651D6A",
    "FF016201BEBC",
    "FF016201BEBC",
    "FF026B05103A7F",
    "FF026B05113A7E",
    "FF026B05023A6D",
    "FF026B05003A6F",
    "FF026B05013A6E",
    "FF026B05123A7D",
    "FF026A01042E4A",
    "FF026A01042E4A",
    "FF016105BDB8",
    "FF016105BDB8",
    "FF00701D7F",
    "FF02100000F093",
    "FF02100000F093",
    "FF02100040F0D3",
    "FF02100040F0D3",
    "FF039A010C00A35D",
    "FF039B050202DEEA",
    "FF039B051200CEE8",
    "FF039B051000CCE8",
    "FF039B051100CDE8",
    "FF039B050000DCE8",
    "FF049B05010100A2FD",
    "FF036C019F41897B",
    "FF039A010C00A35D",
    "FF039102010142C5",
    "FF122F00000122000005092210011B03E801FF010031E6"
  ];

  this.mRfidClient.showStatus("Running");

  /* Send the first command - don't write the second until response received. */
  SignStixSerial.write(this.mConnectionId, this.mInstructionSet[0]);
  this.startRegularUpdate();
};

/* Called whenever a chunk of data is received from the serial interface.
   If there is a series of instructions to send then send the next one. */
ThingMagicRunner.prototype.onDataRead = function(connId, hexData) {
  var bytes = hexToBytes(hexData);
  this.mHandler.receiveData(bytes, bytes.length);

  /* Pluck out the next instruction, if any, and write it to the connection. */
  if (this.mInstructionSet != null) {
    var instruction = this.mInstructionSet[0];
    this.mInstructionSet.shift();
    if (this.mInstructionSet.length == 0) {
      this.mInstructionSet = null;
    }
    SignStixSerial.write(connId, instruction);
  }
};

/* Start a regular processing of the tag database to work out when a tag has been
   removed.  Inform the client that the tag database has been updated. */
ThingMagicRunner.prototype.startRegularUpdate = function() {
  this.mTagDatabase.scanForRemovedTag();
  
  if (this.mConnectionId > 0) {
    var runner = this;
    var interval = this.mRegularUpdateIntervalMs;
    setTimeout(function() {
      runner.startRegularUpdate()
    }, interval);
  }
};

/* Find out if this runner is running. */
ThingMagicRunner.prototype.isRunning = function() {
  return this.mConnectionId > 0;
};

/* Stop the runner. */
ThingMagicRunner.prototype.stop = function() {
  SignStixSerial.close(this.mConnectionId);
  this.mConnectionId = 0;
  this.mRfidClient.showStatus("STOPPED");
};

/******************** class ThingMagicManager ********************/

/* ThingMagicManager accumulates tag data and temperature from an ThingMagic RFID reader. */
function ThingMagicManager(tagDatabase, rfidClient) {
  this.mTagDatabase = tagDatabase; /* TagDatabase */
  this.mRfidClient = rfidClient;
  this.mNumIgnored = 0; /* Number of ignored reads */
}

/* Process a full ThingMagic byte array response.  If the response contains a tag,
   and the read strength is high enough, the tag database will be updated.  */
ThingMagicManager.prototype.processResponse = function(response, length) {
  var verdict = parseResponse(response, length);

  if (verdict == RESPONSE_HAS_TAG) {
    var tagId = parseTagFromResponse(response, length);
    var strength = response[READ_STRENGTH_INDEX];
    
    if (!this.mRfidClient.getShouldIgnoreRead(tagId, strength)) {
      this.mTagDatabase.tagRead(tagId, strength);
    } else {
      this.mNumIgnored++;
      this.mRfidClient.onReadIgnored(this.mNumIgnored);
    }

  } else if (verdict == RESPONSE_HAS_TEMPERATURE) {
    var temp = parseTempFromResponse(response, length);
    this.mRfidClient.onTemperature(temp);
  }
};

/******************** class ThingMagicHandler ********************/

/* ThingMagicHandler assembles data received from a ThingMagic RFID reader into structured responses. 
   The 'proc' argument should be a manager which can process full responses. */
function ThingMagicHandler(proc) {
  this.mProcessor = proc;
  this.mResponse = new ThingMagicResponse();
  this.mData = new DataChunk();
}

/* Receive the argument chunk of data from the RFID reader. When a full response has been
   accumulated, forward it to the processor. */
ThingMagicHandler.prototype.receiveData = function(data, length) {
  this.mData.resetFor(data, length);

  while (this.mData.getBytesRemaining() > 0) {
    if (this.mResponse.isEmpty()) {
      /* Try to find out how big the current message should be. */
      var first = this.mData.getNextByte();
      if (first != 0xFF) {
        return;
      }

      this.mResponse.addFF();

      if (this.mData.getBytesRemaining() <= 0) {
        return;
      } else {
        var len = this.mData.getNextByte();
        this.mResponse.addDataLength(len);
      }

    } else if (this.mResponse.hasOnlyFF()) {
      /* Special case that we only read the opening 'FF' last time. */
      var len = this.mData.getNextByte();
      this.mResponse.addDataLength(len);
    }

    /* So we know the expected length. */
    var bytesToCopy = Math.min(this.mResponse.getBytesToFill(), this.mData.getBytesRemaining());
    this.mData.copyTo(this.mResponse, bytesToCopy);
    if (this.mResponse.isFull()) {
      this.mProcessor.processResponse(this.mResponse.getData(), this.mResponse.getExpectedLength());
      this.mResponse.reset();
    }
  }
};

/******************** ThingMagicResponse ********************/

/* ThingMagicResponse represents a full response to a command sent to a ThingMagic RFID reader.
   Responses are of the form: [FF][Length][OpCode][Status][Data][CRC].  An offset indicates 
   the number of bytes added to the response so far. */
function ThingMagicResponse() {
  this.mOffset = 0;
  this.mExpectedLength = 0;
  this.mData = [];
}

/* Reset the response to make it empty. */
ThingMagicResponse.prototype.reset = function() {
  this.mOffset = 0;
  this.mExpectedLength = 0;
};

/* Add 0xFF as the next (typically first) byte in the response. */
ThingMagicResponse.prototype.addFF = function() {
  this.appendByte(0xFF);
};

/* Add the response length as the next byte in the response.  
  This also sets the expected length for the entire response. */
ThingMagicResponse.prototype.addDataLength = function(length) {
  this.appendByte(length);
  var dataLen = length & 0xFF;
  /* Add 7 for the FF, length byte, op code, status bytes (2), CRC bytes (2). */
  this.mExpectedLength = dataLen + 7; 
};

/* Find out if the response is empty. */
ThingMagicResponse.prototype.isEmpty = function() {
  return this.mOffset == 0;
};

/* Find out if the response only has a single FF in it. */
ThingMagicResponse.prototype.hasOnlyFF = function() {
  return this.mOffset == 1;
};

/* Find out how many bytes of this response are still to be filled. */
ThingMagicResponse.prototype.getBytesToFill = function() {
  return this.mExpectedLength - this.mOffset;
};

/* Increase the offset by the argument amount. */
ThingMagicResponse.prototype.addToOffset = function(extra) {
  this.mOffset += extra;
};

/* Get the current offset i.e. how many bytes the response contains. */
ThingMagicResponse.prototype.getOffset = function() {
  return this.mOffset;
};

/* Find the array of bytes that this response contains. */
ThingMagicResponse.prototype.getData = function() {
  return this.mData;
};

/* Find out if the response is full i.e. it has the same number of bytes as its expected length. */
ThingMagicResponse.prototype.isFull = function() {
  return this.mOffset == this.mExpectedLength && this.mExpectedLength > 0;
};

/* Find the expected length of this response.  This will be 0 initially. */
ThingMagicResponse.prototype.getExpectedLength = function() {
  return this.mExpectedLength;
};

/* Append the argument byte to the response. */
ThingMagicResponse.prototype.appendByte = function(val) {
  this.mData[this.mOffset] = val;
  this.mOffset++;
};

/******************** Generic Library ********************/

/******************** class DataChunk ********************/

/* DataChunk represents a series of data received from an RFID reader.
   An offset determines how much of the chunk has been processed. */
function DataChunk() {
  this.mOffset = 0;
  this.mLength = 0;
  this.mData = null;
}

/* Reset this chunk for processing the argument byte array data. */
DataChunk.prototype.resetFor = function(data, length) {
  this.mOffset = 0;
  this.mData = data;
  this.mLength = length;
};

/* Find out how many bytes are remaining to be processed. */
DataChunk.prototype.getBytesRemaining = function() {
  return this.mLength - this.mOffset;
};

/* Find the next byte and advance the offset. */
DataChunk.prototype.getNextByte = function() {
  var res = this.mData[this.mOffset];
  this.mOffset++;
  return res;
};

/* Copy the argument number of bytes into the argument ThingMagicResponse,
   and advance the offset accordingly. */
DataChunk.prototype.copyTo = function(response, numBytes) {
  var responseData = response.getData();
  var responseOffset = response.getOffset();

  for (i = 0; i < numBytes; i++) {
    responseData[responseOffset + i] = this.mData[this.mOffset + i];
  }

  this.mOffset += numBytes;
  response.addToOffset(numBytes);
};

/******************** class TagDatabase ********************/

/* TagDatabase represents information about tags that have been read. */
function TagDatabase(knownTags, rfidClient) {
  this.mKnownTags = knownTags;
  this.mRfidClient = rfidClient;
  this.mTagMap = {};
  this.mRemoveDetectionPeriodMs = rfidClient.getRemoveDetectionPeriodMs();
  this.mStabilityThreshold = rfidClient.getStabilityThreshold();
}

/* Record the fact that the argument tag has just been read.  This will add a 
   new TagData to the database if necessary.  It will become a candidate for 
   promotion and will be marked as stable if it's had sufficient reads. */
TagDatabase.prototype.tagRead = function(tagId, readStrength) {
  var timeNow = Date.now();
  var data = this.mTagMap[tagId];
  if (data == null) {
    data = new TagData(tagId);
    this.mTagMap[tagId] = data;
    
    /* Look-up the matching 'known tag', if any. */
    var knownTag = this.mKnownTags.getTagWithId(tagId);
    this.mRfidClient.onTagAdded(data, knownTag);
    
    if (knownTag != null) {
      data.setKnownTag(knownTag);
      knownTag.incNumAdded();
    }
  }

  /* Tell the tag data that it has been read. */
  data.tagReadAt(timeNow, readStrength);
  this.mRfidClient.onTagRead(data, this);
  
  /* Update the stability flag. */
  if (!data.isStable() && data.getTotalReads() >= this.mStabilityThreshold) {
    data.setStable(true);
  }
  
  /* If the tag is stable and hasn't been promoted, promote it. */
  if (data.isStable() && !data.isPromotedAdd()) {
    var knownTag = data.getKnownTag();
    if (knownTag != null) {       
      this.mRfidClient.onTagAddedPromote(data, knownTag);
      data.setPromotedAdd(true);
    }
  }
};

/* Scan the tags in the database and see if any need removing i.e. have not been read
   for a given period. */
TagDatabase.prototype.scanForRemovedTag = function() {
  var timeNow = Date.now();
 
  var victims = [];
  var timeLimitMs = this.mRemoveDetectionPeriodMs;

  for (var tagId in this.mTagMap) {
    if (this.mTagMap.hasOwnProperty(tagId)) {
      var data = this.mTagMap[tagId];
      var lastTime = data.getLastReadTime();
      var diff = timeNow - lastTime;
      if (diff > timeLimitMs) {
        victims.push(tagId);
      }
    }
  }

  var numVictims = victims.length;

  /* Remove victim tags from the database. */
  for (i = 0; i < numVictims; i++) {
    var victimId = victims[i];
    var victim = this.mTagMap[victimId];        
    var knownTag = victim.getKnownTag();
    this.mRfidClient.onTagRemoved(victim, knownTag);
    
    if (knownTag != null) {
      knownTag.incNumRemoved();   
      /* Only promote stable known tags. */   
      if (victim.isStable()) {
        this.mRfidClient.onTagRemovedPromote(victim, knownTag);      
      }
    } 
    
    delete this.mTagMap[victimId];   
  }
};

/* Generate a little HTML report of the tags in the database. */
TagDatabase.prototype.getHtmlReport = function() {
  var msg = "<table border=1><tr><th>Name</th><th>Tag</th><th>Reads</th><th>Strength</th><th>Stable</th></tr>";
  for (var tagId in this.mTagMap) {
    if (this.mTagMap.hasOwnProperty(tagId)) {
      var data = this.mTagMap[tagId];
      var id = data.getTagId();
      var kt = data.getKnownTag();
      var name = (kt == null) ? "" : kt.getName();
      var stable = (data.isStable()) ? "Yes" : "No";
      var strength = data.getLastStrength();
      msg = msg + "<tr><td>" + name + "</td><td>" + id + "</td><td>" + data.getTotalReads() + "</td><td>" + strength + "</td><td>" + stable + "</td></tr>";
    }
  }
  msg = msg + "</table>";
  return msg;
};

/******************** class TagData ********************/

/* TagData represents information about a single tag. The 'tagId' is what the RFID reader produces. */
function TagData(tagId) {
  this.mTagId = tagId;
  this.mLastReadTime = 0; /* The last time in milliseconds when this tag was read. */
  this.mLastStrength = 0; /* The strength (0-255) of the last read. */
  this.mTotalReads = 0; /* The total number of reads so far. */
  this.mStable = false; /* True when the number of reads reaches a threshold. */
  this.mPromotedAdd = false; /* True when a tag becomes stable and gets promoted. */
  this.mKnownTag = null; /* Corresponding KnownTag, if any. */
}

/* Record the fact that this tag has been read at the argument time (which is assumed to be increasing). */
TagData.prototype.tagReadAt = function(time, strength) {
  this.mLastReadTime = time;
  this.mLastStrength = strength;
  this.mTotalReads++;
};

/* Find the ID of this tag. */
TagData.prototype.getTagId = function() {
  return this.mTagId;
};

/* Find the last time that this tag was read, or 0 if it has never been read. */
TagData.prototype.getLastReadTime = function() {
  return this.mLastReadTime;
};

/* Find the read strength of the last read, or 0 if it has never been read. */
TagData.prototype.getLastStrength = function() {
  return this.mLastStrength;
};

/* Find the total number of times this tag has been read. */
TagData.prototype.getTotalReads = function() {
  return this.mTotalReads;
};

/* Find out if this tag is stable i.e. has been read several times. */
TagData.prototype.isStable = function() {
  return this.mStable;
};

/* Set whether this tag is stable i.e. has been read several times. */
TagData.prototype.setStable = function(stable) {
  this.mStable = stable;
};

/* Find out if this tag has had its add promoted. */
TagData.prototype.isPromotedAdd = function() {
  return this.mPromotedAdd;
};

/* Set whether this tag has had its add promoted. */
TagData.prototype.setPromotedAdd = function(promotedAdd) {
  this.mPromotedAdd = promotedAdd;
};

/* Set the KnownTag associated with this tag data. */
TagData.prototype.setKnownTag = function(knownTag) {
  this.mKnownTag = knownTag;
};

/* Find the KnownTag associated with this tag data.  May be null. */
TagData.prototype.getKnownTag = function() {
  return this.mKnownTag;
};

/******************** class KnownTag ********************/

/* KnownTag represents an expected tag that we want to respond to.  The 'name' is the 
   user-friendly description.  The 'tagId' is what the RFID reader produces. */
function KnownTag(name, tagId, productCode) {
  this.mName = name;
  this.mTagId = tagId;
  this.mProductCode = productCode;
  this.mNumAdded = 0; /* Number of times this tag has been added to the current set. */
  this.mNumRemoved = 0; /* Number of times this tag has been removed from the current set. */
  this.mExtendedName = this.mName + " [" + this.mTagId + "]";
}

/* Find the ID of this tag. */
KnownTag.prototype.getTagId = function() {
  return this.mTagId;
};

/* Find the user-friendly name of this tag. */
KnownTag.prototype.getName = function() {
  return this.mName;
};

/* Find the product code for this tag. */
KnownTag.prototype.getProductCode = function() {
  return this.mProductCode;
};

/* Find the number of times this tag has been removed from the current set. */
KnownTag.prototype.getNumRemoved = function() {
  return this.mNumRemoved;
};

/* Find the number of times this tag has been added to the current set. */
KnownTag.prototype.getNumAdded = function() {
  return this.mNumAdded;
};

/* Increment the number of times this tag has been removed from the current set. */
KnownTag.prototype.incNumRemoved = function() {
  this.mNumRemoved++;
};

/* Increment the number of times this tag has been added to the current set. */
KnownTag.prototype.incNumAdded = function() {
  this.mNumAdded++;
};

/* Find the extended name of the tag i.e. user-friendly name plus tag ID. */
KnownTag.prototype.getExtendedName = function() {
  return this.mExtendedName;
};

/******************** class KnownTagGroup ********************/

/* A KnownTagGroup is a collection of 'known tags' i.e. tags that we have knowledge about
   and are interested in displaying to the user. */
function KnownTagGroup() {
  this.mTags = new Array(); /* Array of KnownTag. */
}

/* Add the argument known tag to the group. */
KnownTagGroup.prototype.addTag = function(knownTag) {
  this.mTags.push(knownTag);
};

/* Add the argument item to the group (creating a KnownTag for it). */
KnownTagGroup.prototype.addItem = function(itemName, tagId, productCode) {
  var kt = new KnownTag(itemName, tagId, productCode);
  this.addTag(kt);
};

/* Find the number of tags in the group. */
KnownTagGroup.prototype.getNumTags = function() {
  return this.mTags.length;
};

/* Find the tag at the argument index. */
KnownTagGroup.prototype.getTagAt = function(index) {
  return this.mTags[index];
};

/* Find the tag in the group that matches the argument ID, or null if there is no match. */
KnownTagGroup.prototype.getTagWithId = function(tagId) {
  var n = this.getNumTags();
  for (var i = 0; i < n; i++) {
    var kt = this.mTags[i];
    if (kt.getTagId() == tagId) {
      return kt;
    }
  }
  return null;
};

/* Produce a simple HTML report of the tags in the group. */
KnownTagGroup.prototype.getHtmlReport = function() {
  var msg = "<table border=1><tr><th>Name</th><th>Tag</th></tr>";
  for (var i = 0; i < this.getNumTags(); i++) {
    var kt = this.getTagAt(i);
    msg = msg + "<tr><td>" + kt.getName() + "</td><td>" + kt.getTagId() + "</td></tr>";
  }
  msg = msg + "</ol>";
  return msg;
};


/******************** class SerialDevice ********************/

/* A SerialDevice represents a serial device that we connect to in order to 
   access an RFID reader. */
function SerialDevice() {
  this.mVendorId = ""; /* Vendor ID in hex. */
  this.mProductId = ""; /* Product ID in hex. */
  this.mDriverName = ""; /* Name of the driver (as understood by SignStixSerial) for writing to device. */
  this.mBaudRate = 115200;
  this.mStopBits = 1;
  this.mDataBits = 8;
  this.mParity = 0;
}

/* Set the vendor ID to be the hex argument. */
SerialDevice.prototype.setVendorId = function(vendorIdHex) {
  this.mVendorId = vendorIdHex;
};

/* Set the product ID to be the hex argument. */
SerialDevice.prototype.setProductId = function(productIdHex) {
  this.mProductId = productIdHex;
};

/* Set the name of the driver (as understood by SignStixSerial) to use for this device. */
SerialDevice.prototype.setDriverName = function(driverName) {
  this.mDriverName = driverName;
};

/* Set the various serial communications parameters. */
SerialDevice.prototype.setCommsConfig = function(baudRate, stopBits, dataBits, parity) {
  this.mBaudRate = baudRate;
  this.mStopBits = stopBits;
  this.mDataBits = dataBits;
  this.mParity = parity;
};

/* Get the vendor ID (in hex). */
SerialDevice.prototype.getVendorId = function() {
  return this.mVendorId;
};

/* Get the product ID (in hex). */
SerialDevice.prototype.getProductId = function() {
  return this.mProductId;
};

/* Get the name of the driver to use in SignStixSerial. */
SerialDevice.prototype.getDriverName = function() {
  return this.mDriverName;
};

/* Get the baud rate. */
SerialDevice.prototype.getBaudRate = function() {
  return this.mBaudRate;
};

/* Get the number of stop bits. */
SerialDevice.prototype.getStopBits = function() {
  return this.mStopBits;
};

/* Get the number of data bits. */
SerialDevice.prototype.getDataBits = function() {
  return this.mDataBits;
};

/* Get the parity. */
SerialDevice.prototype.getParity = function() {
  return this.mParity;
};

/******************** Utility functions ********************/

/* Convert the argument string of data in hex bytes to an array of bytes. */
function hexToBytes(hex) {
  var hexLength = hex.length;
  for (var bytes = [], c = 0; c < hexLength; c += 2)
    bytes.push(parseInt(hex.substr(c, 2), 16));
  return bytes;
}

/* Convert the bytes in the argument byte array at the argument offset into a hex string. */
function bytesToHex(bytes, offset, length) {
  for (var hex = [], i = 0; i < length; i++) {
    hex.push((bytes[offset + i] >>> 4).toString(16).toUpperCase());
    hex.push((bytes[offset + i] & 0xF).toString(16).toUpperCase());
  }
  return hex.join("");
}  

    
/******************** MAIN PROGRAM ********************/

var gTagDatabase = null; /* Stores information about the tags that have been read. */
var gRfidClient = new RfidClient();
var gRunner = null;

/* Attempt to find the desired USB device and get permission for it. */
function startSerial() {
  if (gRunner != null && gRunner.isRunning()) {
    gRfidClient.showStatus("Already started");
    return;
  } 
  
  gRfidClient.showStatus("Starting");
  gRfidClient.onReadPowerSet(gRfidClient.getReadPower());
  
  /* Set up the known tags and tags database. */
  var knownTags = new KnownTagGroup();
  gRfidClient.populateTags(knownTags);
  gRfidClient.onKnownTags(knownTags);
  gRfidClient.onReadIgnored(0);
  gTagDatabase = new TagDatabase(knownTags, gRfidClient);

  /* Try to find our device */
  var devicesInfo = SignStixSerial.getDevicesInfo();
  gRfidClient.onFoundDevices(devicesInfo);

  var devices = JSON.parse(devicesInfo);
  var device = null;
  
  var serialDevice = gRfidClient.getSerialDevice();
  var vendor = serialDevice.getVendorId();
  var product = serialDevice.getProductId();

  for (var i = 0; i < devices.length; i++) {
    if (devices[i].vendorId == vendor && devices[i].productId == product) {
      device = devices[i];
    }
  }

  var deviceStr = "[Device not found]";
  if (device != null) {
    deviceStr = device.devicePath;
  }

  gRfidClient.onFoundDevice(deviceStr);

  if (device != null) {
    SignStixSerial.requestPermission(device.devicePath, "onPermissionGranted");
  }
}

/* Called when permission has been granted to use the device with the argument path. 
   Initiate a sequence of commands which will start the RFID reader scanning. */
function onPermissionGranted(devicePath, success) {
  gRfidClient.onPermission(success);
  
  var serialDevice = gRfidClient.getSerialDevice();
  var driver = serialDevice.getDriverName();
  var baud = serialDevice.getBaudRate();
  var stopBits = serialDevice.getStopBits();
  var dataBits = serialDevice.getDataBits();
  var parity = serialDevice.getParity();
 
  var connectionId = SignStixSerial.connect(devicePath, driver, baud, stopBits, dataBits, parity);
  gRfidClient.onConnection(connectionId);
  
  gRunner = new ThingMagicRunner(gTagDatabase, gRfidClient, connectionId);
  gRunner.start();
}

/* Stop the serial test (if it's running). */
function stopSerial() {
  if (gRunner == null || !gRunner.isRunning()) {
    /* If the test is not running then do nothing. */
    gRfidClient.showStatus("Already stopped");
    return;
  }

  gRunner.stop();
}


/******************** HTML ********************/

/* Create the HTML that the call-backs will update. */
var heading = document.createElement('h1');
heading.innerHTML = "SignStix RFID Monitor";
document.body.appendChild(heading);

var assume = document.createElement('p');
assume.innerHTML = "This assumes that a ThingMagic Nano RFID reader has been connected to a USB port via a CH34xUART serial device.";
document.body.appendChild(assume);

/* Status */
var s = document.createElement('p');
document.body.appendChild(s);

var stat = document.createElement('span');
stat.id = "status";
s.appendChild(stat);

/* Devices info */
var p = document.createElement('p');
document.body.appendChild(p);

var deviceInfo = document.createElement('span');
deviceInfo.id = "deviceInfo";
p.appendChild(deviceInfo);

/* Feedback */
var f = document.createElement('h2');
document.body.appendChild(f);

var feedback = document.createElement('span');
feedback.id = "feedback";
f.appendChild(feedback);

/* Table with all the stats and tag info. */
var t = document.createElement('table');
t.setAttribute('border', 1);
document.body.appendChild(t);

var r0 = t.insertRow(0);

var c0_0 = r0.insertCell(0);
var foundDevice = document.createElement('span');
foundDevice.id = "foundDevice";
c0_0.appendChild(foundDevice);

var c0_1 = r0.insertCell(1);
var permission = document.createElement('span');
permission.id = "permission";
c0_1.appendChild(permission);

var r1 = t.insertRow(1);
 
var c1_0 = r1.insertCell(0);
var connection = document.createElement('span');
connection.id = "connection";
c1_0.appendChild(connection);

var c1_1 = r1.insertCell(1);
var temperature = document.createElement('span');
temperature.id = "temperature";
c1_1.appendChild(temperature);

var r2 = t.insertRow(2);

var c2_0 = r2.insertCell(0);
var readPower = document.createElement('span');
readPower.id = "readPower";
c2_0.appendChild(readPower);

var c2_1 = r2.insertCell(1);
var ignored = document.createElement('span');
ignored.id = "ignored";
c2_1.appendChild(ignored);

var r3 = t.insertRow(3);

var c3_0 = r3.insertCell(0);
c3_0.innerHTML = "Known tags:";

var c3_1 = r3.insertCell(1);
var knownTags = document.createElement('span');
knownTags.id = "knownTags";
c3_1.appendChild(knownTags);

var r4 = t.insertRow(4);

var c4_0 = r4.insertCell(0);
c4_0.innerHTML = "Found tags:";

var c4_1 = r4.insertCell(1);
var tagDump = document.createElement('span');
tagDump.id = "tagDump";
c4_1.appendChild(tagDump);


/* Start it all going. */
startSerial();
