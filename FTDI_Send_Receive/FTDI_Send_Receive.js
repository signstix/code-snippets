/* SignStix Serial - FTDI Send/Receive */

/* This is a Javascript Extension that can run run in a SignStix Web View.
It assumes that an FTDI device is connected over USB.
Strings of bytes representing ASCII-encoded strings are sent on a regular basis,
and any received data is converted to ASCII and displayed. */

const VENDOR_ID = '403'; // You may need to change this for your particular device.
const PRODUCT_ID = '6001'; // You may need to change this for your particular device.
const READ_INTERVAL_MS = 3000; // How often in milliseconds to trigger a send.

var sendCount = 1; // Incremented on each send.

/* Find the USB device info that matches the argument vendor and product IDs. */
function getDeviceInfoFor(vendorId, productId) {
  var devicesInfo = SignStixSerial.getDevicesInfo();
  var devices = JSON.parse(devicesInfo);
  document.write(devicesInfo);
  
  for (var i = 0; i < devices.length; i++) {
    var device = devices[i];
    if (device.vendorId == vendorId && device.productId == productId) {
      return device;
    }
  }
  return null;
}

/* Convert the argument string of data in hex bytes to an array of bytes. */
function hexToBytes(hex) {
  var hexLength = hex.length;
  for (var bytes = [], c = 0; c < hexLength; c += 2)
    bytes.push(parseInt(hex.substr(c, 2), 16));
  return bytes;
}

/* Convert the bytes in the argument byte array at the argument offset into a hex string. */
function bytesToHex(bytes, offset, length) {
  for (var hex = [], i = 0; i < length; i++) {
    hex.push((bytes[offset + i] >>> 4).toString(16).toUpperCase());
    hex.push((bytes[offset + i] & 0xF).toString(16).toUpperCase());
  }
  return hex.join("");
}  

/* Convert the argument text to ASCII hex. */
function textToHex(text) {
  var len = text.length;
  var bytes = new Array();
  for (var i = 0; i < len; i++) {
    var char = text.charCodeAt(i);
    bytes.push(char);
  }
  var hex = bytesToHex(bytes, 0, len);
  return hex;
}

/* Convert the argument hex string to text, assuming the hex values are ASCII. */
function hexToText(hex) {
  var bytes = hexToBytes(hex);
  var text = '';
  for (var i = 0; i < bytes.length; i++) {
    var b = bytes[i];
    var char = String.fromCharCode(b);
    text = text + char;
  }
  return text;
}

/* Called when any data arrives. */
function globalOnDataRead(connId, hexData) {
  var text = hexToText(hexData);
  document.write(text);
}

/* Send the next message to the connection identified by the argument connection ID. */
function sendMessage(connectionId) {
  var message = "Hello " + sendCount;
  sendCount++;
  var msgHex = textToHex(message);
  document.write("<br/>Sending hex: " + msgHex + " text:" + message + "<br/>");
  SignStixSerial.write(connectionId, msgHex);
}

/* Called when permission to access the proximity device is determined. */
function onPermissionGranted(devicePath, success) {
  document.write("<br/>Permission granted: " + success);
  var driver = "usb";
  var baud = 9600;
  var stopBits = 1;
  var dataBits = 8;
  var parity = 0;
  var connectionId = SignStixSerial.connect(devicePath, driver, baud, stopBits, dataBits, parity);  
  
  SignStixSerial.startReading(connectionId, "globalOnDataRead");
  
  // Arrange for messages to go regularly.
  setInterval(function() {
    sendMessage(connectionId);
  }, READ_INTERVAL_MS);
}


// MAIN PROGRAM

document.write("<br/>SignStix Serial - FTDI Send/Receive");

var deviceInfo = getDeviceInfoFor(VENDOR_ID, PRODUCT_ID);
if (deviceInfo == null) {
  SignStixDebug.error("No matching FTDI device found");
}
SignStixSerial.requestPermission(deviceInfo.devicePath, "onPermissionGranted");
