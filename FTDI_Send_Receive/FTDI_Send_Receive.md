# FTDI Send/Receive #

This is a test/demo for the SignStix serial interface. 

Requires SignStix Player version 2.11.0R8 or above.

It assumes that the SignStix device is connected to 
an FTDI device plugged into a USB port on the SignStix device.

It runs in a Web View and regularly sends simple ASCII 
messages to the FTDI device.  Any responses are 
interpreted as ASCII and displayed.
