# Proximity Sensor #

This is a test/demo for the SignStix serial interface. 

Requires SignStix Player version 2.11.0R8 or above.

It assumes that the SignStix device is connected to 
a SignStix-supplied proximity sensor plugged into a 
USB port on the SignStix device.

It runs in a Web View and displays messages on the 
screen whenever an object comes within 20 cm of the sensor.

