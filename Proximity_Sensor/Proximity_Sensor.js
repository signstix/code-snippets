/* SignStix Serial - Proximity Sensor Demo */

/* Requires SignStix Player 2.11.0R8+ */
/* Requires a SignStix-supplied proximity sensor */

const LIMIT_CM = 20; // Come within this limit and it will tell you.
const READ_INTERVAL_MS = 500; // How often in milliseconds to trigger a proximity read.

/* Find the device info that matches the argument vendor and product IDs. */
function getDeviceInfoFor(vendorId, productId) {
  var devicesInfo = SignStixSerial.getDevicesInfo();
  var devices = JSON.parse(devicesInfo);
  var device = null;
  
  for (var i = 0; i < devices.length; i++) {
    var device = devices[i];
    if (device.vendorId == vendorId && device.productId == productId) {
      return device;
    }
  }
  return null;
}

/* Convert the argument string of data in hex bytes to an array of bytes. */
function hexToBytes(hex) {
  var hexLength = hex.length;
  for (var bytes = [], c = 0; c < hexLength; c += 2)
    bytes.push(parseInt(hex.substr(c, 2), 16));
  return bytes;
}

/* Convert the hex string response from the sensor into a float number of centimetres.
In the input, each hex byte represents the ASCII code of a decimal digit, or the
decimal point (full stop), or an end-of-line or carriage-return. */
function responseToDistance(hex) {
  var bytes = hexToBytes(hex);
  var distance = '';
  for (var i = 0; i < bytes.length; i++) {
    var b = bytes[i];
    if (b == 46) {
      distance = distance + ".";
    } else if (b == 10 || b == 13) {
      // Ignore newlines in the data.
    } else {
      var number = b - 48; // '0' is 48 decimal in ASCII.
      distance = distance + number;
    }
  }
  return parseFloat(distance);
}

/* Called when any data arrives. */
function globalOnDataRead(connId, hexData) {
  var distance = responseToDistance(hexData);
  if (distance < LIMIT_CM) {
    document.write("<br/>You're only " + distance.toFixed(0) + " cm away from me - back off!");  
  }
}

/* Called when permission to access the proximity device is determined. */
function onPermissionGranted(devicePath, success) {
  document.write("<br/>Permission granted: " + success);
  var driver = "usb";
  var baud = 9600;
  var stopBits = 1;
  var dataBits = 8;
  var parity = 0;
  var connectionId = SignStixSerial.connect(devicePath, driver, baud, stopBits, dataBits, parity);  
  
  SignStixSerial.startReading(connectionId, "globalOnDataRead");
  
  // Arrange for measurements to occur every second.
  setInterval(function() {
    // Character 'r' is 72 is ASCII hex.
    SignStixSerial.write(connectionId, "72");
  }, READ_INTERVAL_MS);
}


// MAIN PROGRAM

document.write("<br/>SignStix Serial Test - Proximity Sensor.");
document.write("<br/>Come too close to the sensor and it will tell you to back off!");
var vendor = '1B4F';
var product = '9206';
var deviceInfo = getDeviceInfoFor(vendor, product);
if (deviceInfo == null) {
  SignStixDebug.error("No matching proximity device found");
}
SignStixSerial.requestPermission(deviceInfo.devicePath, "onPermissionGranted");


