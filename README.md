# JS Code snippets for Creator

This repository is a store for any code examples we may want to
keep that can be used in the JS Extension of creator.  This code is provided under the ISC licence, see [LICENSE](LICENSE)

## Twitter
In the [twitterTimeline](Twitter/twitterTimeline.js) and [latestTweets](Twitter/latestTweets.js) examples `<company-name-here>` should be replaced
with your actual company name as it is in your twitter username.
For instance in the case of SignStix `<company-name-here>`
would simply become `SignStix`. Once you have done this the
code can simply be copied and pasted into the `JS Extension`
text box.  You can specify `about:blank` as the URL of the webview layer.

In some places there are options that are configurable by the user
these are highlighted by a comment on the line above.

You can also add timelines for Twitter [Collections](https://developer.twitter.com/en/docs/tweets/curate-a-collection/overview/overview), [Lists](https://help.twitter.com/en/using-twitter/twitter-lists) and Likes.  
In each case, copy the URL for the timeline, and paste into the ```link.href``` in the JS Extension.  For example, to see the Tweets liked by SignStix: ````link.href="https://twitter.com/SignStix/likes"````


## Tableau
Embed Visualisations from Tableau into your webviews.  
Simply add the view you want in the `TARGET_VIEW`.  If you have
a private Tableau Server, change the `YOUR_SERVER`.  
Then copy-and-paste the code into the `JS Extension` text box
in your webview.  You can specify `about:blank` as the URL.  
Try changing the `TARGET_VIEW` to 'MLBAnalytics/PITCHfx' for
some baseball statistics.

The sample is based on the API documented at
https://onlinehelp.tableau.com/current/api/js_api/en-us/JavaScriptAPI/js_api.htm
and modified to be a pure javascript solution.

## RFID Monitor
This is a substantial RFID reading application demonstrating the
use of the `SignStixSerial` interface.  It's a standalone piece of
JavaScript code that can be deployed in a JavaScript Extension with
an `about:blank` URL.  
See the [documentation](./RFID_Monitor/RFID_Monitor.md) for more details.

## Proximity Sensor
This is a fairly small example demonstrating the use of the
`SignStixSerial` interface to discover when an object comes
within range of a proximity sensor.  It's a standalone piece of
JavaScript code that can be deployed in a JavaScript Extension with
an `about:blank` URL.  
See the [documentation](./Proximity_Sensor/Proximity_Sensor.md) for more details.

## FTDI Send/Receive
This is a fairly small example demonstrating the use of the
`SignStixSerial` interface to exchange character data with
an FTDI device over USB.  It's a standalone piece of JavaScript
code that can be deployed in a JavaScript Extension with an
`about:blank` URL.  
See the [documentation](./FTDI_Send_Receive/FTDI_Send_Receive.md) for more details.
