// Modify the 2 variables as appropriate
var TARGET_VIEW = 'TheU_S_CommutingLandscape/TheU_S_CommutingLandscape';
var YOUR_SERVER = 'public.tableau.com';


// Implementation code below. You should not need to chnage anything below this point.
var divElement = document.createElement('div');
divElement.id = 'vizContainer';
document.body.appendChild(divElement);

var scriptElement = document.createElement('script');
// Note: direct link to API minor version required for async loading.
scriptElement.src = 'https://' + YOUR_SERVER + '/javascripts/api/tableau-2.2.1.min.js';
scriptElement.async = true;
scriptElement.onload = function() {
  var containerDiv = document.getElementById("vizContainer"),
      url = "https://" + YOUR_SERVER + "/views/" + TARGET_VIEW,
      options = {
          hideTabs: true,
          hideToolbar: true,
          height: '100%',
          width: '100%',
      };

  var viz = new tableau.Viz(containerDiv, url, options);
 };

document.body.appendChild(scriptElement);
